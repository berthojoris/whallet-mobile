# Whallet App for Android Mobile

Whallet is simple management for personal finance.

[http://www.whallet.com/](http://www.whallet.com/)

# Components

- LungoJS [http://lungo.tapquo.com/](http://lungo.tapquo.com/)
- QuoJS [http://quojs.tapquo.com/](http://quojs.tapquo.com/)
- Monocle [http://monocle.tapquo.com/](http://monocle.tapquo.com/)
- accounting.js [http://josscrowcroft.github.io/accounting.js/](http://josscrowcroft.github.io/accounting.js/)
- dateJS [http://www.datejs.com/](http://www.datejs.com/)
- Chart.js [http://www.chartjs.org/](http://www.chartjs.org/)

# Credits

  **Co-Founder and CTO: Jose Vicente Martinez Mendoza**

  Twitter: [@jomarmen](http://www.twitter.com/jomarmen/)

  **Co-Founder and CEO: Adrian Capote**

  Twitter: [@adriancapote](http://www.twitter.com/adriancapote/)

  **Co-Founder and Mobile Developer: Santi Ruiz**

  Twitter: [@santiruiz84](http://www.twitter.com/santiruiz84/)