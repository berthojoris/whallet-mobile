class __Model.Movement extends Monocle.Model

    @fields "id", "title", "description", "done", "created_at", "category", "date", "tags", "amount", "type", "formatDate"

    @kind_of: ->
        if movement.category == 10 or movement.category == "10" then "income" else "expense"