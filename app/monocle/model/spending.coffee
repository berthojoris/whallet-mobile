class __Model.Spending extends Monocle.Model

    @fields "description", "amount", "percentage", "position", "category"