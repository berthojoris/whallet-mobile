class __View.MovementItemList extends Monocle.View

    container: "ul#movements"

    template:
        """
        <li id="{{id}}" class="selectable" style="margin-bottom: 0px">
            <span class="icon whallet {{category}}"></span>
            <div class="right text {{type}}"><h4>{{amount}}</h4></div>
            <strong><span class="text bold">{{tags}} </span></strong>
            <small>
                {{description}}
            </small>
        </li>
        """

    events:
        "hold li h4"    : "onDelete"
        "tap li"        : "onEdit"

    onEdit: (event) ->
        console.log "onEdit.view"
        console.log @model

        Lungo.dom("a#deleteMovement").removeClass()

        Lungo.dom("a#income").removeClass()
        Lungo.dom("a#expense").removeClass()

        if @model.category == "add-money"
            Lungo.dom("a#income").addClass("active-income")
        else
            Lungo.dom("a#expense").addClass("active-expense")

        Lungo.dom("section#NewMovement > article div.category span").removeClass()
        Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet " + @model.category)

        Lungo.dom("section#NewMovement > article span#amount")[0].innerText = "€ " + @model.amount.replace(" €","")
        description = ""
        if @model.description != "No hay descripción"
            description = @model.description

        Lungo.dom("section#NewMovement > article input#txt-description")[0].value = description
        Lungo.dom("section#NewMovement > article input#txt-tag")[0].value = @model.tags

        currentDate = new Date(@model.date)
        dd = currentDate.getDate()
        mm = currentDate.getMonth() + 1
        yyyy = currentDate.getFullYear()
        if (dd < 10)
            dd = "0" + dd

        if (mm < 10)
            mm = "0" + mm

        currentDate = dd + '/' + mm + '/' + yyyy
        Lungo.dom("section#NewMovement > article span#txt-date")[0].innerText = currentDate

        Lungo.Data.Storage.persistent("movementID", @model.id)
        Lungo.Router.section("NewMovement")
    onDelete: (event) ->
        console.log "onDelete.view"
        #if navegador.onLine
        @remove()


        #Lungo.Notification.confirm({
        #    icon: 'warning',
        #    title: 'Are you sure that want to delete the movement?',
        #    description: null,
        #    accept: {
        #        icon: 'checkmark',
        #        label: 'Accept',
        #        callback: null #confirmDelete(@)
        #    },
        #    cancel: {
        #        icon: 'close',
        #        label: 'Cancel',
        #        callback: null
        #    }
        #});

    #confirmDelete= (movement) ->
        #console.log "confirmDelete"
        #movement.remove()