class __View.SpendingItem extends Monocle.View

    container: "ul#distribution_table"

    template:
        """
            <li style="margin-bottom: 0px; padding: 5px" class="text-color_{{position}}">                
                <span class="icon whallet {{category}}"></span>                    
                <strong style="display:block">
                        <div class="right" style="width: 60px; text-align:right">{{percentage}}</div>
                        <div class="right" style="">{{amount}}</div>                        
                    </div>                    
                </strong>
                <strong>{{description}}</strong>
                <small>&nbsp</small>                
            </li>
        """