class DistributionCtrl extends Monocle.Controller

    events:
        "load section#distribution"  : "onLoadDistribution"        

    elements:
        "#distribution_date"            :   "distribution_date"
        "#distribution_expense_month"   :   "distribution_expense_month"
        "#distribution_data_table"      :   "distribution_data_table"

    onChangeDate: (event) ->
        console.log "onChangeDate"


        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))
        arrayMonths = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        yearCalendarMobile  = fechaCalendario.getFullYear()
        monthCalendarMobile = fechaCalendario.getMonth()

        textMonthCalendarMobile = arrayMonths[monthCalendarMobile].slice(0,3)

        template  = new String('<div id="modal_calendario">')
        template += '     <form>'
        template += '     <fieldset style="border-bottom: 2px solid #0093D5">'
        template += '                 <span id="screen-date" style="font-size: 1.4em; height:40px; margin-top:15px" class="text thin left"><abbr>'+ Helpers.getElegantDate(fechaCalendario)+'</abbr></span>'
        template += '     </fieldset>'
        template += '     <div>'
        template += '         <div id="contenedor_day_calendario" class="hidden fecha_calendario fecha_calendario_left">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masDayCalendarMobile" class="signo_calendario" onclick=Calendar.addDay()><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="dayCalendarMobile" class="numero_calendario">1</div>';
        template += '             </div>';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="menosDayCalendarMobile" class="signo_calendario" onclick=Calendar.removeDay()><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         </div>';
        template += '         <div id="contenedor_month_calendario" class="fecha_calendario">'
        template += '             <div class="contenedor_signo_calendario">'
        template += '                 <div id="masMonthCalendarMobile" class="signo_calendario" onclick=Calendar.addMonth()><span class="icon android aup"></span></div>'
        template += '             </div>'
        template += '             <div class="contenedor_numero_calendario">'
        template += '                 <div id="monthCalendarMobile" class="numero_calendario">'+textMonthCalendarMobile+'</div>'
        template += '             </div>'
        template += '             <div id="anterior_month" class="contenedor_signo_calendario">'
        template += '                 <div id="menosMonthCalendarMobile" class="signo_calendario" onclick=Calendar.removeMonth()><span class="icon android adown"></span></div>'
        template += '             </div>'
        template += '         </div>'
        template += '         <div id="contenedor_year_calendario" class="fecha_calendario fecha_calendario_right">'
        template += '             <div class="contenedor_signo_calendario">'
        template += '                 <div id="masYearCalendarMobile" class="signo_calendario" onclick=Calendar.addYear()><span class="icon android aup"></span></div>'
        template += '             </div>'
        template += '             <div class="contenedor_numero_calendario">'
        template += '                 <div id="yearCalendarMobile" class="numero_calendario">'+yearCalendarMobile+'</div>'
        template += '             </div>'
        template += '             <div class="contenedor_signo_calendario">'
        template += '                 <div id="menosYearCalendarMobile" class="signo_calendario" onclick=Calendar.removeYear()><span class="icon android adown"></span></div>'
        template += '             </div>'
        template += '         </div>'
        template += '     </div>'
        template += ' </div></form>'

        Lungo.Notification.confirm({
            icon: '',
            title: 'Selecciona una fecha',
            description: template,
            accept: {
                icon: 'checkmark',
                label: 'Aceptar',
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()

                    Lungo.Data.Storage.persistent("movements", null)
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#distribution_date")[0].innerHTML = Helpers.getFormatDate(date)
                    
                    Lungo.Data.Storage.persistent("expenseDistribution", null)
                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/dashboard/spending.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("token"),
                        month: date.getMonth() + 1,
                        year: date.getFullYear()

                    result = Lungo.Service.post(url, post_data, spendingResponse)
            },
            cancel: {
                icon: 'close',
                label: 'Cancelar',
                callback: null
            }
        })

    onLoadDistribution: (event) ->
        console.log "onLoadDistribution"

        expenseDistribution = Lungo.Data.Storage.persistent("expenseDistribution")

        if expenseDistribution isnt null
            spendingResponse expenseDistribution
        else
            #if navegador.onLine
            Lungo.Notification.show()
            date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
            url = "http://www.whallet.com/api/v1/dashboard/spending.json"
            post_data =
                token: Lungo.Data.Storage.persistent("token")


            result = Lungo.Service.post(url, post_data, spendingResponse)

    spendingResponse= (response) ->
        console.log "spendingResponse"

        expenseDistribution = Lungo.Data.Storage.persistent("expenseDistribution")
        if expenseDistribution is null
            Lungo.Data.Storage.persistent("expenseDistribution", response)
            expenseDistribution = response
        # clear list
        $$("ul#distribution_table").html(' ')
        $$("h1#expense_month").html(' ')

        if expenseDistribution isnt null and expenseDistribution.data.length == 0
            $$("div#distribution_no_data").removeClass()
            $$("div#distribution_data").removeClass()
            $$("div#distribution_data").addClass("hidden")
            @distribution_data_table.className = "hidden"
            @distribution_date.innerHTML = Helpers.getFormatDate(new Date(response.date))
        else
            $$("div#distribution_no_data").removeClass()
            $$("div#distribution_data").removeClass()
            $$("div#distribution_no_data").addClass("hidden")
            @distribution_data_table.className = ""
            Lungo.Data.Storage.persistent("date", response.date)
            Lungo.Data.Storage.persistent("expenses", response.total)

            colors= new Array("#E3563A","#A8C656","#19AA5F","#00AFD8","#514E93","#E25695","#C35049","#E66F59","#EBA543",'#D9D6D1','#D4D84F','#7ABA5C','#68B0A2','#5E8BB4','#C3C2BD','#8D8D8B','#5D5F5A','#B04B73','#C1B295','#977550')

            @distribution_date.innerHTML = Helpers.getFormatDate(new Date(response.date))
            @distribution_expense_month.innerHTML =  Helpers.getFormatNumber(response.total,"€")

            count = 0
            distributionData = []
            for s in response.data
                spendingModel = __Model.Spending.create
                    category: Helpers.getCategoryName(s['category']),
                    description: s['description'],
                    amount:  Helpers.getFormatNumber(s['amount'],"€"),
                    percentage: s['percentage'] + " %",
                    position: count

                view = new __View.SpendingItem model: spendingModel
                view.append spendingModel

                data = {
                    value: s['percentage'],
                    color: colors[count]
                }
                distributionData.push data

                count = count + 1

            canvas = Lungo.dom("canvas#distribution_chart")[0]
            container_canvas = Lungo.dom("section#distribution div#main")[0]
            canvas.width = container_canvas.clientWidth
            canvas.height = container_canvas.clientHeight

            chart = (canvas).getContext("2d")

            optionsChart = {
                animation: false
            }
            myDoughnut = new Chart(chart).Pie(distributionData, optionsChart)

        Lungo.Notification.hide()

controller_distribution = new DistributionCtrl "section#distribution"