class MovementsCtrl extends Monocle.Controller

    events:
        "load section#movements"    : "onLoadMovements"
        "tap h4#movements_date"     : "onChangeDate"
        "tap span.icon.menu"        : "onChangeDate"

    elements:
        "#movements_date"           :   "movements_date"

    constructor: ->
        super
        __Model.Movement.bind "destroy",  @bindMovementDestroy

    bindMovementDestroy: (movement) ->
        console.log "bindMovementDestroy"
        #document.getElementById("delete_tag_"+ event.currentTarget.id).className = "right tag cancel"

        url = "http://www.whallet.com/api/v1/movement/delete.json"
        date = new Date()
        post_data =
            token: Lungo.Data.Storage.persistent("token"),
            id: movement.id

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, deleteResponse)

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            "Error",
            "Algo ha ido mal.",
            "cancel",
            3,
            null
        )

    deleteResponse= (response) ->
        console.log "deleteResponse"

        Lungo.Data.Storage.persistent("balances", null)
        Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)

        Lungo.Notification.success(
            "Success",
            "Movimiento eliminado.",
            "check",
            3,
            null
        )

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: 'Selecciona una fecha',
            description:  Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: 'Aceptar',
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#movements_date")[0].innerHTML = Helpers.getFormatDate(date)
                    
                    Lungo.Data.Storage.persistent("movements", null)
                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/movements.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("token"),
                        year: date.getFullYear(),
                        month: date.getMonth()+1

                    result = Lungo.Service.post(url, post_data, movementsResponse)
            },
            cancel: {
                icon: 'close',
                label: 'Cancelar',
                callback: null
            }
        })

    onLoadMovements: (event) ->
        console.log "onLoadMovements"

        movements = Lungo.Data.Storage.persistent("movements")

        if movements isnt null
            movementsResponse movements
        else
            Lungo.Notification.show()
            url = "http://www.whallet.com/api/v1/movements.json"
            date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
            post_data =
                token: Lungo.Data.Storage.persistent("token"),
                year: date.getFullYear(),
                month: date.getMonth()+1

            result = Lungo.Service.post(url, post_data, movementsResponse)

    movementsResponse= (response) ->
        console.log "movementsResponse"

        movements = Lungo.Data.Storage.persistent("movements")
        date = Lungo.Data.Storage.persistent("dateSelected")
        @movements_date.innerHTML = Helpers.getFormatDate(new Date(date))

        if movements is null
            Lungo.Data.Storage.persistent("movements", response)
            movements = response

        # clear list
        $$("ul#movements").html(' ')
        $$("div#distribution_no_data").addClass("hidden")
        $$("article#movements").addClass("indented")
        if movements.length == 0
            $$("div#distribution_no_data").removeClass()
            $$("article#movements").removeClass("indented")
        else

        formatDate = ""
        for movement in response
            description = if movement['description'] then movement['description'] else 'No hay descripción'
            date = new Date(movement['date'])
            actualFormatDate = Helpers.getElegantDate(date)

            formatDay = ""
            if date.getDate() < 10
                formatDay = "0#{date.getDate()}"
            else
                formatDay = "#{date.getDate()}"

            type = "expense"
            if movement.category == 10 or movement.category == "10" then type = "income"

            movementModel = __Model.Movement.create
                description: description,
                category: Helpers.getCategoryName(movement['category']),
                date: date,
                formatDate: actualFormatDate,
                tags: movement['tags'],
                amount:  Helpers.getFormatNumber(movement['amount'], "€"),
                type: type,
                done: false,
                id: movement['_id']

            if formatDate isnt actualFormatDate
                formatDate = actualFormatDate
                viewHeader = new __View.MovementHeaderList model: movementModel
                viewHeader.append movementModel

            view = new __View.MovementItemList model: movementModel
            view.append movementModel

        Lungo.Notification.hide()

controller_movements = new MovementsCtrl "section#movements"