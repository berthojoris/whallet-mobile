class MonitorCtrl extends Monocle.Controller

    events:
        "load section#monitor"  : "onLoadMonitor"
        "tap h4#monitor_date"   : "onChangeDate"
        "tap span.icon.menu"    : "onChangeDate"

    elements:
        "#monitor_date"     :   "monitor_date"
        "#balance_month"    :   "balance_month"
        "#expense_month"    :   "expense_month"
        "#income_month"     :   "income_month"
        "#bar"              :   "bar"

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: 'Selecciona una fecha',
            description: Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: 'Aceptar',
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("movements", null)
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#monitor_date")[0].innerHTML = Helpers.getFormatDate(date)
                    balances = Lungo.Data.Storage.persistent("balances")

                    if balances isnt null
                        balanceResponse(balances)
                    else
                        Lungo.Notification.show()
                        url = "http://www.whallet.com/api/v1/dashboard/balance.json"
                        post_data = token: Lungo.Data.Storage.persistent("token")
                        result = Lungo.Service.post(url, post_data, balanceResponse)
            },
            cancel: {
                icon: 'close',
                label: 'Cancelar',
                callback: null
            }
        })

    onLoadMonitor: (event) ->
        console.log "onLoadMonitor"

        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        @monitor_date[0].innerHTML = Helpers.getFormatDate(date)

        balances = Lungo.Data.Storage.persistent("balances")

        if balances isnt null
            balanceResponse(balances)
        else
            Lungo.Notification.show()
            url = "http://www.whallet.com/api/v1/dashboard/balance.json"
            post_data = token: Lungo.Data.Storage.persistent("token")
            result = Lungo.Service.post(url, post_data, balanceResponse)

    balanceResponse= (response) ->
        console.log "balanceResponse"

        balances = Lungo.Data.Storage.persistent("balances")
        if balances is null
            Lungo.Data.Storage.persistent("balances", response)

        m_names = new Array("ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC")

        incomes = []
        expenses = []
        dates = []

        #date = new Date()   
        noData = 0     
        date = new Date(Lungo.Data.Storage.persistent("dateSelected"))
        for dataBalance in response
            dateBalance = new Date(dataBalance.date)
            if((dateBalance.getMonth() == date.getMonth()) and (dateBalance.getFullYear() == date.getFullYear()))

                @balance_month.innerHTML = Helpers.getFormatNumber(dataBalance.data.balance, "€")
                if dataBalance.data.balance > 0
                    @balance_month.innerHTML = "+"+ Helpers.getFormatNumber(dataBalance.data.balance, "€")

                @expense_month.innerHTML = "<span class='icon whallet arrow-down-2 red'></span>" + Helpers.getFormatNumber(dataBalance.data.expense, "€")
                @income_month.innerHTML = "<span class='icon whallet arrow-up green'></span>"  + Helpers.getFormatNumber(dataBalance.data.income, "€")

                #percentage_income = (dataBalance.data.income*100)/(dataBalance.data.income+dataBalance.data.expense)
                if dataBalance.data.balance <= 0
                    percentage_income = 0
                else
                    percentage_income = (dataBalance.data.balance*100)/dataBalance.data.income
                @bar.style.width = percentage_income + "%";

            dates.push m_names[dateBalance.getMonth()]
            incomes.push dataBalance.data.income
            expenses.push dataBalance.data.expense  

            noData += dataBalance.data.income + dataBalance.data.expense
        
        if noData > 0
            $$("div#evolution_no_data").addClass("hidden")
            $$("div#evolutionChart").removeClass()
            
            lineChartData = {
                labels : dates,
                datasets : [
                    {
                        fillColor : "rgba(244,246,237,0.5)",
                        strokeColor : "rgba(168,198,86,1)",
                        pointColor : "rgba(168,198,86,1)",
                        pointStrokeColor : "#fff",
                        data : incomes
                    },
                    {
                        fillColor : "rgba(247,236,235,0.5)",
                        strokeColor : "rgba(209,72,54,1)",
                        pointColor : "rgba(209,72,54,1)",
                        pointStrokeColor : "#fff",
                        data : expenses
                    }
                ]
            }

            optionsChart = {
                scaleOverlay: true,
                bezierCurve: false,
                animation: false
            }
            canvas = Lungo.dom("#canvas")[0]
            container_canvas = Lungo.dom("#evolutionChart")[0]
            canvas.width = container_canvas.clientWidth
            canvas.height = container_canvas.clientHeight

            chart = (Lungo.dom("canvas#canvas")[0]).getContext("2d")
            myLine = new Chart(chart).Line(lineChartData, optionsChart)
        else
            $$("div#evolutionChart").addClass("hidden")
            $$("div#evolution_no_data").removeClass()

        Lungo.Notification.hide()

controller_monitor = new MonitorCtrl "section#monitor"