class SplashCtrl extends Monocle.Controller

    events:
        "load section#splash"               : "onLoadSplash"

    onLoadSplash: (event) ->
        console.log "onLoadSplash"
        
        if navigator.onLine 
            console.log "hay conexión"
            date = new Date()
            Lungo.Data.Storage.persistent("dateSelected", null)
            Lungo.Data.Storage.persistent("dateSelected", date)

            tokenStored = Lungo.Data.Storage.persistent("token")

            if tokenStored isnt null
                # Recovery tags
                url = "http://www.whallet.com/api/v1/helper/tags.json"
                post_data = token: tokenStored
                result = Lungo.Service.post(url, post_data, helperTagsResponse)

                #  2 - Spending -> Storage
                Lungo.Data.Storage.persistent("balances", null)

                url = "http://www.whallet.com/api/v1/dashboard/balance.json"
                post_data = token: tokenStored
                result = Lungo.Service.post(url, post_data, balanceResponse)

                #  3 - Expense Distribution
                Lungo.Data.Storage.persistent("expenseDistribution", null)
                url = "http://www.whallet.com/api/v1/dashboard/spending.json"
                post_data =
                    token: tokenStored

                result = Lungo.Service.post(url, post_data, spendingResponse)

                #  4- Movements -> Storage
                Lungo.Data.Storage.persistent("movements", null)
                url = "http://www.whallet.com/api/v1/movements.json"

                post_data =
                    token: tokenStored,
                    year: date.getFullYear(),
                    month: date.getMonth()+1

                result = Lungo.Service.post(url, post_data, movementsResponse)
            else
                Lungo.Router.section("home")
        else
            console.log "no hay conexión"

            Lungo.Notification.error(
                "Se produjo un error desconocido",
                "Compruebe la conexión a internet por favor.",
                "broadcast",
                5,
                null
            )

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            "Se produjo un error desconocido",
            "Compruebe la conexión a internet por favor.",
            "broadcast",
            5,
            null
        )


    movementsResponse= (response) ->
        console.log "Splash.movementsResponse-----------------------------------------"        

        console.log("response: %o", response)
        if movements isnt null
            console.log("respuesta not null")
            Lungo.Data.Storage.persistent("movements", response)
            Lungo.Router.section("monitor")

        else
            console.log("respuesta null")
            Lungo.Router.section("home")

    spendingResponse= (response) ->
        console.log "Splash.spendingResponse"

        Lungo.Data.Storage.persistent("expenseDistribution", response)


    balanceResponse= (response) ->
        console.log "Splash.balanceResponse"

        Lungo.Data.Storage.persistent("balances", response)

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

controller_splash = new SplashCtrl "section#splash"
