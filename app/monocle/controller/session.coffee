class SessionCtrl extends Monocle.Controller

    events:
        "load section#home"                 : "onLoadHome"
        "load section#login"                : "onLoadLogin"
        "load section#signup"               : "onLoadSignup"
        "tap a[data-action=login]"          : "onLogin"
        "tap a[data-action=logout]"         : "onLogout"
        "tap a[data-action=create-account]" : "onCreateAccount"

    elements:
        "#login_email"      : "login_email"
        "#login_password"   : "login_password"
        "#signup_email"     : "signup_email"
        "#signup_password"  : "signup_password"

    onLoadSignup: (event) ->
        console.log "onLoadSignup"

    onLoadLogin: (event) ->
        console.log "onLoadLogin"
        tokenStored = Lungo.Data.Storage.persistent("token")

        if tokenStored isnt null

            #user = Lungo.Data.Storage.persistent("user")
            #Lungo.dom("aside#options > header div.title")[0].innerHTML = user

            # Recovery tags
            url = "http://www.whallet.com/api/v1/helper/tags.json"
            post_data = token: tokenStored
            result = Lungo.Service.post(url, post_data, helperTagsResponse)

            Lungo.Router.section("monitor")

    onLoadHome: (event) ->
        console.log "onLoadHome"

    onCreateAccount: (event) ->
        console.log "onCreateAccount"

        url = "http://www.whallet.com/api/v1/registrations.json"
        post_data =
            email: @signup_email.val(),
            password: @signup_password.val(),
            password_confirmation: @signup_password.val()

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, registrationResponse)

    onLogout: (event) ->
        Lungo.Data.Storage.persistent("token",null)
        Lungo.Data.Storage.persistent("user",null)
        Lungo.Data.Storage.persistent("balances", null)
        Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)

        Lungo.dom("section#login #login_email").val("")
        Lungo.dom("section#login #login_password").val("")

        __Model.Tag.destroyAll()

        Lungo.Router.section("home")

    onLogin: (event) ->
        console.log "onLogin"
        tokenStored = Lungo.Data.Storage.persistent("token")

        Lungo.Notification.show()
        if tokenStored == null
            console.log "No tenemos el token llamamos al servicio"
            console.log tokenStored

            url = "http://www.whallet.com/api/v1/tokens.json"
            post_data = email: @login_email.val(), password: @login_password.val()
            Lungo.Service.Settings.error = errorResponse

            result = Lungo.Service.post(url, post_data, sessionResponse)

        else
            console.log "Token: " + tokenStored
            Lungo.Router.section("monitor")

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            "Error",
            "Hay algún problema con tu login.",
            "cancel",
            3,
            null
        );

    registrationResponse= (response) ->
        console.log "registrationResponse"

        url = "http://www.whallet.com/api/v1/tokens.json"
        post_data =
            email: Lungo.dom("section#signup #signup_email").val(),
            password: Lungo.dom("section#signup #signup_password").val()

        result = Lungo.Service.post(url, post_data, sessionResponse)

    sessionResponse= (response) ->
        console.log "sessionResponse"
        console.log "token es " + response.token

        if response.token?
            Lungo.Data.Storage.persistent("token", response.token)
            email = Lungo.dom("section#login #login_email").val()
            Lungo.Data.Storage.persistent("user", email)

            #Lungo.dom("aside#options > header div.title")[0].innerHTML = email

            # Recovery tags
            url = "http://www.whallet.com/api/v1/helper/tags.json"
            post_data = token: response.token
            result = Lungo.Service.post(url, post_data, helperTagsResponse)
        else
            Lungo.Notification.error(
                "Error",
                "Hay algún problema con tu login.",
                "cancel",
                3,
                null
            );

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

        Lungo.Router.section("monitor")

controller_login = new SessionCtrl "section#login"
controller_signup = new SessionCtrl "section#signup"
controller_home = new SessionCtrl "section#home"
controller_splash = new SessionCtrl "section#splash"
controller_aside = new SessionCtrl "aside#options"
