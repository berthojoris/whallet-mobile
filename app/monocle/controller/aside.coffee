class AsideCtrl extends Monocle.Controller

    events:
        "tap li"    :   "onChangeSection"

    onChangeSection: (event) ->
        console.log "onChangeSection"

        Lungo.dom("li.active").removeClass()
        Lungo.dom("li#"+event.currentTarget.id).addClass('active')

        Lungo.Router.section(checkSectionNoData(event.currentTarget.id))

    checkSectionNoData= (section) ->
        if section == "movements"
            movements = Lungo.Data.Storage.persistent("movements")

            #if movements isnt null and movements.length == 0
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"            
        else if section == "distribution"
            distribution = Lungo.Data.Storage.persistent("expenseDistribution")

            #if distribution isnt null and distribution.data.length == 0
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"            
        else if section == "monitor" 
            balances = Lungo.Data.Storage.persistent("balances")

            #if balances is null
            #    Lungo.Data.Storage.persistent("sectionReturnNoData", section)
            #    section = "emptyData"

        Lungo.Router.aside(section, "options");

        section

controller_aside = new AsideCtrl "aside#options"