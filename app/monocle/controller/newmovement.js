// Generated by CoffeeScript 1.6.2
(function() {
  var NewMovementCtrl, controller_newMovement,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  NewMovementCtrl = (function(_super) {
    var afterNotification, deleteResponse, errorResponse, helperTagsResponse, saveResponse;

    __extends(NewMovementCtrl, _super);

    NewMovementCtrl.prototype.events = {
      "load section#NewMovement": "onLoadNewMovement",
      "tap nav.groupbar a": "onChangeKindOf",
      "tap a[data-action=save]": "onSave",
      "tap a[data-action=cancel]": "onCancel",
      "tap a[data-action=delete]": "onDelete"
    };

    NewMovementCtrl.prototype.elements = {
      "#amount": "amount",
      "#category": "category",
      "#txt-tag": "tags",
      "#txt-date": "date",
      "#txt-description": "description"
    };

    function NewMovementCtrl() {
      NewMovementCtrl.__super__.constructor.apply(this, arguments);
      console.log("Constructor");
    }

    NewMovementCtrl.prototype.onDelete = function(event) {
      var date, post_data, result, url;

      url = "http://www.whallet.com/api/v1/movement/delete.json";
      date = new Date();
      post_data = {
        token: Lungo.Data.Storage.persistent("token"),
        id: Lungo.Data.Storage.persistent("movementID")
      };
      Lungo.Service.Settings.error = errorResponse;
      return result = Lungo.Service.post(url, post_data, deleteResponse);
    };

    NewMovementCtrl.prototype.onLoadNewMovement = function(event) {
      var bulletsList, categoryClass, categoryId, first, numberTagsByCategory, smallTagsList, tag, tags, tagsByCategory, tagsList, _i, _j, _len, _len1;

      console.log("onLoadNewMovement");
      categoryClass = Lungo.dom("span#category")[0].className.split(" ");
      categoryId = Helpers.getCategoryId(categoryClass[2]);
      Lungo.dom("div#tags")[0].innerHTML = "";
      Lungo.dom("ul#bullets")[0].innerHTML = "";
      bulletsList = "";
      bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>";
      tags = __Model.Tag.all();
      tagsList = "";
      first = true;
      tagsByCategory = [];
      for (_i = 0, _len = tags.length; _i < _len; _i++) {
        tag = tags[_i];
        if (tag.category === categoryId) {
          tagsByCategory.push(tag);
        }
      }
      numberTagsByCategory = tagsByCategory.length;
      while (tagsByCategory.length) {
        smallTagsList = tagsByCategory.splice(0, 6);
        if (first) {
          bulletsList += "<li class='selected'><span class='bullet'>o</span></li>";
        } else {
          bulletsList += "<li class=''><span class='bullet'>o</span></li>";
        }
        first = false;
        tagsList += "<div align='center'>";
        for (_j = 0, _len1 = smallTagsList.length; _j < _len1; _j++) {
          tag = smallTagsList[_j];
          tagsList += "<a href='#' class='button-tag' onclick=\"document.getElementById('txt-tag').value='" + tag.description + "'\">" + tag.description + "</a>";
        }
        tagsList += '</div>';
      }
      Lungo.dom("ul#bullets")[0].style.cssText = "";
      if (numberTagsByCategory < 6) {
        Lungo.dom("ul#bullets")[0].style.cssText = "display:none";
      } else {
        Lungo.dom("ul#bullets")[0].style.cssText = "display: block;margin:10px";
      }
      bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>";
      Lungo.dom("ul#bullets")[0].innerHTML = bulletsList;
      return Lungo.dom("div#tags")[0].innerHTML = tagsList;
    };

    NewMovementCtrl.prototype.onCancel = function(event) {
      console.log("onCancel");
      Lungo.Data.Storage.persistent("movementID", null);
      return Lungo.Router.back();
    };

    NewMovementCtrl.prototype.onSave = function(event) {
      var categoryClass, categoryId, date, dateSplit, day, month, movementID, post_data, result, tag, today, url, year;

      console.log("onSave");
      Lungo.Notification.show();
      date = this.date[0].innerText;
      if (date === "") {
        today = new Date();
        day = dateMovement.getDate();
        month = dateMovement.getMonth() + 1;
        year = yeardateMovement.getFullYear();
      } else {
        dateSplit = date.split("/");
        day = dateSplit[0];
        month = dateSplit[1];
        year = dateSplit[2];
      }
      tag = this.tags.val();
      if (tag === "") {
        tag = "no tag";
      }
      categoryClass = Lungo.dom("span#category")[0].className.split(" ");
      categoryId = getCategoryId(categoryClass[2]);
      movementID = Lungo.Data.Storage.persistent("movementID");
      if (movementID !== null) {
        url = "http://www.whallet.com/api/v1/movement/update.json";
        post_data = {
          token: Lungo.Data.Storage.persistent("token"),
          year: year,
          month: month,
          day: day,
          amount: this.amount[0].innerText.replace("€", ""),
          description: this.description.val(),
          category: categoryId,
          tags: tag,
          id: movementID
        };
      } else {
        url = "http://www.whallet.com/api/v1/movement/new.json";
        post_data = {
          token: Lungo.Data.Storage.persistent("token"),
          year: year,
          month: month,
          day: day,
          amount: this.amount[0].innerText.replace("€", ""),
          description: this.description.val(),
          category: categoryId,
          tags: tag
        };
      }
      Lungo.Service.Settings.error = errorResponse;
      result = Lungo.Service.post(url, post_data, saveResponse);
      url = "http://www.whallet.com/api/v1/helper/tags.json";
      post_data = {
        token: Lungo.Data.Storage.persistent("token")
      };
      return result = Lungo.Service.post(url, post_data, helperTagsResponse);
    };

    errorResponse = function(type, xhr) {
      console.log("errorResponse");
      Lungo.Notification.hide();
      return Lungo.Notification.error("Error", "Something wrong with your movement.", "cancel", 3, null);
    };

    deleteResponse = function(response) {
      console.log("deleteResponse");
      Lungo.Data.Storage.persistent("balances", null);
      Lungo.Data.Storage.persistent("expenseDistribution", null);
      Lungo.Data.Storage.persistent("movements", null);
      Lungo.Data.Storage.persistent("movementID", null);
      return Lungo.Notification.success("Éxito", "Movimiento eliminado.", "check", 3, afterNotification);
    };

    afterNotification = function() {
      return Lungo.Router.back();
    };

    saveResponse = function(response) {
      var sectionBack;

      console.log("saveResponse");
      Lungo.Data.Storage.persistent("balances", null);
      Lungo.Data.Storage.persistent("expenseDistribution", null);
      Lungo.Data.Storage.persistent("movements", null);
      Lungo.Data.Storage.persistent("movementID", null);
      sectionBack = Lungo.Data.Storage.persistent("sectionReturnNoData");
      if (sectionBack !== null) {
        Lungo.Data.Storage.persistent("sectionReturnNoData", null);
        return Lungo.Router.section(sectionBack);
      } else {
        return Lungo.Router.back();
      }
    };

    helperTagsResponse = function(response) {
      var dataResponse, tag, tagModel, _i, _len, _results;

      console.log("helperTagsResponse");
      __Model.Tag.destroyAll();
      _results = [];
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        dataResponse = response[_i];
        _results.push((function() {
          var _j, _len1, _ref, _results1;

          _ref = dataResponse.tags;
          _results1 = [];
          for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
            tag = _ref[_j];
            _results1.push(tagModel = __Model.Tag.create({
              description: tag.name,
              category: dataResponse.category
            }));
          }
          return _results1;
        })());
      }
      return _results;
    };

    NewMovementCtrl.prototype.onChangeKindOf = function(event) {
      var bulletsList, categoryId, first, smallTagsList, tag, tags, tagsByCategory, tagsList, _i, _j, _len, _len1;

      console.log("onChangeKingOf");
      categoryId = 0;
      Lungo.dom("a#income").removeClass();
      Lungo.dom("a#expense").removeClass();
      if (event.srcElement.id === "income") {
        Lungo.dom("a#income").addClass("active-income");
        Lungo.dom("section#NewMovement > article div.category span").removeClass();
        Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet add-money");
        categoryId = 10;
      } else {
        Lungo.dom("a#expense").addClass("active-expense");
        Lungo.dom("section#NewMovement > article div.category span").removeClass();
        Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet tag-2");
      }
      Lungo.dom("div#tags")[0].innerHTML = "";
      Lungo.dom("ul#bullets")[0].innerHTML = "";
      bulletsList = "";
      bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>";
      tags = __Model.Tag.all();
      tagsList = "";
      first = true;
      tagsByCategory = [];
      for (_i = 0, _len = tags.length; _i < _len; _i++) {
        tag = tags[_i];
        if (tag.category === categoryId) {
          tagsByCategory.push(tag);
        }
      }
      while (tagsByCategory.length) {
        smallTagsList = tagsByCategory.splice(0, 6);
        if (first) {
          bulletsList += "<li class='selected'><span class='bullet'>o</span></li>";
        } else {
          bulletsList += "<li class=''><span class='bullet'>o</span></li>";
        }
        first = false;
        tagsList += "<div align='center'>";
        for (_j = 0, _len1 = smallTagsList.length; _j < _len1; _j++) {
          tag = smallTagsList[_j];
          tagsList += "<a href='#' class='button-tag' onclick=document.getElementById('txt-tag').value='" + tag.description + "'>" + tag.description + "</a>";
        }
        tagsList += '</div>';
      }
      bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>";
      Lungo.dom("div#tags")[0].innerHTML = tagsList;
      return Lungo.dom("ul#bullets")[0].innerHTML = bulletsList;
    };

    return NewMovementCtrl;

  })(Monocle.Controller);

  controller_newMovement = new NewMovementCtrl("section#NewMovement");

}).call(this);
