class NewMovementCtrl extends Monocle.Controller

    events:
        "load section#NewMovement"  : "onLoadNewMovement"
        "tap nav.groupbar a"        : "onChangeKindOf"
        "tap a[data-action=save]"   : "onSave"
        "tap a[data-action=cancel]" : "onCancel"
        "tap a[data-action=delete]" : "onDelete"

    elements:
        "#amount"           : "amount"
        "#category"         : "category"
        "#txt-tag"          : "tags"
        "#txt-date"         : "date"
        "#txt-description"  : "description"

    constructor: ->
        super
        console.log "Constructor"

    onDelete: (event) ->
        url = "http://www.whallet.com/api/v1/movement/delete.json"
        date = new Date()
        post_data =
            token: Lungo.Data.Storage.persistent("token"),
            id: Lungo.Data.Storage.persistent("movementID")

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, deleteResponse)

    onLoadNewMovement: (event) ->
        console.log "onLoadNewMovement"

        categoryClass = Lungo.dom("span#category")[0].className.split(" ")
        categoryId = Helpers.getCategoryId(categoryClass[2])

        Lungo.dom("div#tags")[0].innerHTML = ""
        Lungo.dom("ul#bullets")[0].innerHTML = ""
        bulletsList = ""
        bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>"

        tags = __Model.Tag.all()
        tagsList = ""
        first = true
        tagsByCategory = []
        for tag in tags
            if tag.category == categoryId
                tagsByCategory.push tag
        numberTagsByCategory = tagsByCategory.length
        while tagsByCategory.length
            smallTagsList = tagsByCategory.splice(0,6)
            if first
                bulletsList += "<li class='selected'><span class='bullet'>o</span></li>"
            else
                bulletsList += "<li class=''><span class='bullet'>o</span></li>"
            first = false
            tagsList += "<div align='center'>"            
            for tag in smallTagsList
                tagsList += "<a href='#' class='button-tag' onclick=\"document.getElementById('txt-tag').value='"+tag.description+"'\">"+ tag.description + "</a>"                
            tagsList += '</div>'

        Lungo.dom("ul#bullets")[0].style.cssText = ""
        if numberTagsByCategory < 6        
            Lungo.dom("ul#bullets")[0].style.cssText = "display:none"
        else
            Lungo.dom("ul#bullets")[0].style.cssText = "display: block;margin:10px"

        bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>"
        Lungo.dom("ul#bullets")[0].innerHTML = bulletsList
        Lungo.dom("div#tags")[0].innerHTML = tagsList
        


    onCancel: (event) ->
        console.log "onCancel"
        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Router.back()

    onSave: (event) ->
        console.log "onSave"

        #if navigation.onLine
        Lungo.Notification.show()

        date = @date[0].innerText
        if date == ""
            today = new Date()
            day = dateMovement.getDate()
            month = dateMovement.getMonth()+1
            year = yeardateMovement.getFullYear()
        else
            dateSplit = date.split("/")
            day = dateSplit[0]
            month = dateSplit[1]
            year = dateSplit[2]

        tag = @tags.val()
        if tag == ""
            tag = "no tag"

        categoryClass = Lungo.dom("span#category")[0].className.split(" ")
        categoryId = getCategoryId(categoryClass[2])

        movementID = Lungo.Data.Storage.persistent("movementID")

        if movementID isnt null
            url = "http://www.whallet.com/api/v1/movement/update.json"
            post_data =
                token: Lungo.Data.Storage.persistent("token"),
                year: year,
                month: month,
                day: day,
                amount: @amount[0].innerText.replace("€",""),
                description: @description.val(),
                category: categoryId,
                tags: tag,
                id: movementID

        else
            url = "http://www.whallet.com/api/v1/movement/new.json"
            post_data =
                token: Lungo.Data.Storage.persistent("token"),
                year: year,
                month: month,
                day: day,
                amount: @amount[0].innerText.replace("€",""),
                description: @description.val(),
                category: categoryId,
                tags: tag

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, saveResponse)

        url = "http://www.whallet.com/api/v1/helper/tags.json"
        post_data = token: Lungo.Data.Storage.persistent("token")
        result = Lungo.Service.post(url, post_data, helperTagsResponse)

    errorResponse= (type, xhr) ->
        console.log "errorResponse"

        Lungo.Notification.hide()

        Lungo.Notification.error(
            "Error",
            "Something wrong with your movement.",
            "cancel",
            3,
            null
        );

    deleteResponse= (response) ->
        console.log "deleteResponse"

        Lungo.Data.Storage.persistent("balances", null)
        Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)
        Lungo.Data.Storage.persistent("movementID", null)

        Lungo.Notification.success(
            "Éxito",
            "Movimiento eliminado.",
            "check",
            3,
            afterNotification 
        )

    afterNotification= () ->
        Lungo.Router.back()

    saveResponse= (response) ->
        console.log "saveResponse"

        Lungo.Data.Storage.persistent("balances", null)
        Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)
        Lungo.Data.Storage.persistent("movementID", null)

        sectionBack = Lungo.Data.Storage.persistent("sectionReturnNoData")

        if sectionBack isnt null
            Lungo.Data.Storage.persistent("sectionReturnNoData", null)
            Lungo.Router.section(sectionBack)            
        else
            Lungo.Router.back()

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        __Model.Tag.destroyAll()
        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

    onChangeKindOf: (event)->
        console.log "onChangeKingOf"

        categoryId = 0
        Lungo.dom("a#income").removeClass()
        Lungo.dom("a#expense").removeClass()


        if(event.srcElement.id is "income")
            Lungo.dom("a#income").addClass("active-income")
            Lungo.dom("section#NewMovement > article div.category span").removeClass();
            Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet add-money");
            categoryId = 10
        else
            Lungo.dom("a#expense").addClass("active-expense")
            Lungo.dom("section#NewMovement > article div.category span").removeClass();
            Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet tag-2");

        Lungo.dom("div#tags")[0].innerHTML = ""
        Lungo.dom("ul#bullets")[0].innerHTML = ""
        bulletsList = ""
        bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>"

        tags = __Model.Tag.all()
        tagsList = ""
        first = true
        tagsByCategory = []
        for tag in tags
            if tag.category == categoryId
                tagsByCategory.push tag

        while tagsByCategory.length
            smallTagsList = tagsByCategory.splice(0,6)
            if first
                bulletsList += "<li class='selected'><span class='bullet'>o</span></li>"
            else
                bulletsList += "<li class=''><span class='bullet'>o</span></li>"
            first = false
            tagsList += "<div align='center'>"
            for tag in smallTagsList
                tagsList += "<a href='#' class='button-tag' onclick=document.getElementById('txt-tag').value='"+tag.description+"'>"+ tag.description + "</a>"
            tagsList += '</div>'

        bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>"
        Lungo.dom("div#tags")[0].innerHTML = tagsList
        Lungo.dom("ul#bullets")[0].innerHTML = bulletsList

controller_newMovement = new NewMovementCtrl "section#NewMovement"