var decimalAdded = false;
var MAX_DECIMAL = 2;
var countDecimal = 0;

Lungo.Events.init({
    'tap a#newMovement': function(){
        console.log("NewMovement");

        Lungo.dom("a#deleteMovement").removeClass();
        Lungo.dom("a#deleteMovement").addClass("hidden");
        Lungo.Data.Storage.persistent("movementID", null);
        Lungo.dom("a#income").removeClass();
        Lungo.dom("a#expense").removeClass();
        Lungo.dom("a#expense").addClass("active-expense");
        Lungo.dom("section#NewMovement > article div.category span").removeClass();
        Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet tag-2");

        Lungo.dom("section#NewMovement > article span#amount")[0].innerText = "€ 0,00";
        Lungo.dom("section#NewMovement > article input#txt-description")[0].value = "";
        Lungo.dom("section#NewMovement > article input#txt-tag")[0].value = "";

        currentDate = new Date();
        dd = currentDate.getDate();
        mm = currentDate.getMonth() + 1;
        yyyy = currentDate.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        currentDate = dd + '/' + mm + '/' + yyyy;
        Lungo.dom("section#NewMovement > article span#txt-date")[0].innerText = currentDate;

        Lungo.Router.section("NewMovement");
    },
    'load section#NewMovement': function(){
        var el = $$('[data-control=carousel]')[0];
        carousel = Lungo.Element.Carousel(el, function(index, element) {
            Lungo.dom("[data-control='carousel'] > ul li.selected").removeClass();
            var selector = "[data-control='carousel'] > ul li:nth-child("+(index+2)+")";
            Lungo.dom(selector).addClass("selected");
        });
    },
    'tap section#NewMovement > article span#txt-date': function(){
        console.log("Tap txt-date");

        var inputFecha = Lungo.dom("section#NewMovement > article span#txt-date")[0].innerText;
        var separatorDate = "/";
        if(inputFecha === ""){ fechaCalendario = new Date(); }
        else{
            valueInputFecha = inputFecha.split(separatorDate);
            fechaCalendario = new Date(valueInputFecha[2], valueInputFecha[1]-1, valueInputFecha[0]);
        }

        Lungo.Notification.confirm({
            icon: '',
            title: 'Selecciona una fecha',
            description: Helpers.getTemplateSelectDayMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: 'Aceptar',
                callback: function(){
                    var inputScreen = Lungo.dom("#screen-date")[0];
                    date = Calendar.getActualDate();

                    dd = date.getDate();
                    mm = date.getMonth() + 1;
                    yyyy = date.getFullYear();

                    dd = ("0" + dd).slice(-2);
                    mm = ("0" + mm).slice(-2);


                    date = dd + '/' + mm + '/' + yyyy;

                    Lungo.dom("section#NewMovement > article span#txt-date")[0].innerText = date;
                }
            },
            cancel: {
                icon: 'close',
                label: 'Cancelar',
                callback: null
            }
        });

    },
    'tap section#NewMovement > article a[data-direction="left"]': function(){
        carousel.prev();
    },
    'tap section#NewMovement > article a[data-direction="right"]': function(){
        carousel.next();
    },
    'tap section#NewMovement > article div#carousel a.button': function(){
        console.log("button tag");
        Lungo.dom("#txt-tag").val("");
        Lungo.dom("#txt-tag").val(this.innerHTML);
    },
    'tap section#NewMovement > article div.category': function(){
        categoryClass = Lungo.dom("span#category")[0].className.split(" ");
        if(categoryClass[2] !== "add-money")
        {
            Lungo.Router.section("newCategory");
        }

    },
    'tap section#newCategory > article li': function(event){
        Lungo.dom("section#NewMovement > article div.category span").removeClass();
        Lungo.dom("section#NewMovement > article div.category span").addClass("icon whallet "+ this.id);

        setTimeout((function() {
            return Lungo.Router.back();
        }), 500);
    },
    'tap section#NewMovement > article div.amount': function(){
        Lungo.dom("#screen")[0].textContent = "0,00";
        Lungo.Router.section("newAmount");
    },
    'tap section#newAmount > article#Form-Amount form div span' : function (event){
        var inputScreen = Lungo.dom("#screen")[0];
        var valueScreen = inputScreen.textContent;
        var valuesScreen = valueScreen.split(',');
        var integerPart = valuesScreen[0];
        var decimalPart = valuesScreen[1];

        var valueBtnPussed = this.textContent;

        if(event.srcElement.id == 'delete'){
            integerPart = '0';
            decimalPart = '00';
            decimalAdded = false;
            countDecimal = 0;
        }
        else if(valueBtnPussed == ','){
            if(!decimalAdded){
                decimalAdded = true;
            }
        }
        else{
            if(countDecimal < 2)
            {
                if(!decimalAdded)
                {
                    if(integerPart == '0')
                    {
                        integerPart = valueBtnPussed;
                    }
                    else
                    {
                        integerPart += valueBtnPussed;
                    }
                }
                else
                {
                    countDecimal = countDecimal + 1;
                    if(countDecimal == 1)
                    {
                        decimalPart = valueBtnPussed + '0';
                    }
                    else if(countDecimal == 2)
                    {
                        decimalPart = decimalPart.substring(0,1) + valueBtnPussed;
                    }

                }
            }
        }

        inputScreen.innerHTML = integerPart + ',' + decimalPart;
    },
    'tap section#newAmount > article a#accept' : function (event){
        var inputScreen = Lungo.dom("#screen")[0];

        Lungo.dom("section#NewMovement > article div.amount span")[0].innerHTML = "€" + inputScreen.textContent;
        Lungo.dom("#screen")[0].textContent = "0,00";
        countDecimal = 0;
        decimalAdded = false;
        Lungo.Router.back();
    }
});

document.onkeypress=function(e){
    var esIE = (document.all);
    var esNS = (document.layers);
    tecla = (esIE) ? event.keyCode : e.which;

    if(tecla == 13){
        return false;
      }
};
