var Helpers = (function(lng, undefined){
    var m_names = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var short_m_names = new Array("ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC");
    var d_names = new Array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");

    miclick = function(){
        console.log("click");
        console.log(Lungo.Dom("#dayCalendarMobile"));
    };

    categoryTranslate = function(category) {
        var category_name;
         if (category === "Recibos") {
            category_name = "Recibos";
          } else if (category === "Compra") {
            category_name = "Shop";
          } else if (category === "Comida") {
            category_name = "Food";
          } else if (category === "Educación") {
            category_name = "Education";
          } else if (category === "Transporte") {
            category_name = "Transport";
          } else if (category === "Ocio") {
            category_name = "Entertaiment";
          } else if (category === "Personal") {
            category_name = "Personal";
          } else if (category === "Salud") {
            category_name = "Health";
          } else if (category === "Casa") {
            category_name = "Home";
          } else if (category === "add-money") {
            category_name = "Income";
          } else {
            category_name = "Others";
          }
        return category_name;
    };

    getCategoryName = function(category) {
        var category_name;
        if (category === "1" || category === 1) {
            category_name = "recibo";
        } else if (category === "2" || category === 2) {
            category_name = "compra";
        } else if (category === "3" || category === 3) {
            category_name = "food";
        } else if (category === "4" || category === 4) {
            category_name = "educacion";
        } else if (category === "5" || category === 5) {
            category_name = "transporte";
        } else if (category === "6" || category === 6) {
            category_name = "ocio";
        } else if (category === "7" || category === 7) {
            category_name = "personal";
        } else if (category === "8" || category === 8) {
            category_name = "salud";
        } else if (category === "9" || category === 9) {
            category_name = "casita";
        } else if (category === "10" || category === 10) {
            category_name = "add-money";
        } else {
            category_name = "wtag";
        }
        return category_name;
    };

    getCategoryId = function(category) {
      var category_id;
      if (category === "recibo") {
        category_id = 1;
      } else if (category === "compra") {
        category_id = 2;
      } else if (category === "food") {
        category_id = 3;
      } else if (category === "educacion") {
        category_id = 4;
      } else if (category === "transporte") {
        category_id = 5;
      } else if (category === "ocio") {
        category_id = 6;
      } else if (category === "personal") {
        category_id = 7;
      } else if (category === "salud") {
        category_id = 8;
      } else if (category === "casita") {
        category_id = 9;
      } else if (category === "add-money") {
        category_id = 10;
      } else {
        category_id = 0;
      }
      return category_id;
    };

    getFormatDate = function(date) {
      return m_names[date.getMonth()] + " " + date.getFullYear();
    };

    getElegantDate = function(date) {
        day = date.getDay();
        number_day = date.getDate();
        month = date.getMonth();
        year = date.getFullYear();

        return d_names[day] + ", " + number_day + " " + m_names[month] + " " + year;
    };

    getFormatNumber = function(number, currency) {
        return accounting.formatMoney(number, "", 2, ".", ",") + " " + currency;
    };


    getTemplateSelectDayMonthYear = function(date){
        yearCalendarMobile  = date.getFullYear();
        monthCalendarMobile = date.getMonth();
        dayCalendarMobile   = date.getDate();

        //Variables String del calendario
        textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0,3);
        textDayCalendarMobile   = ("0"+dayCalendarMobile).slice(-2);

        template  = new String('<div id="modal_calendario">');
        template += '     <form>';
        template += '     <fieldset style="border-bottom: 2px solid #0093D5">';
        template += '         <span id="screen-date" style="font-size: 1.4em; height:40px; margin-top:15px" class="text thin left"><abbr>'+ Helpers.getElegantDate(date) +'</abbr></span>';
        template += '     </fieldset>';
        template += '     <div>';
        template += '         <div id="contenedor_day_calendario" class="fecha_calendario fecha_calendario_left">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masDayCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addDay())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="dayCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ textDayCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="menosDayCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeDay())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         </div>';
        template += '         <div id="contenedor_month_calendario" class="fecha_calendario">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addMonth())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="monthCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ textMonthCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div id="anterior_month" class="contenedor_signo_calendario">';
        template += '                 <div id="menosMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeMonth())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         </div>';
        template += '         <div id="contenedor_year_calendario" class="fecha_calendario fecha_calendario_right">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masYearCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addYear())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="yearCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ yearCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="menosYearCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeYear())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         <div style="clear:both;"></div>';
        template += '         </div>';
        template += '     </div>';
        template += ' </div></form>';

        return template;
    }

    getTemplateSelectMonthYear = function(date) {
      yearCalendarMobile = date.getFullYear();
      monthCalendarMobile = date.getMonth();
      textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0, 3);
      
      template = new String('<div id="modal_calendario">');
      template += '     <form style="width: 100%">';
      template += '     <fieldset style="border-bottom: 2px solid #0093D5">';
      template += '                 <span id="screen-date" style="font-size: 1.4em; height:40px; margin-top:15px" class="text thin left"><abbr>' + Helpers.getFormatDate(date) + '</abbr></span>';
      template += '     </fieldset>';
      template += '     <div>';
      template += '     <div style="margin: 0px auto; width: 145px;">';
      template += '         <div id="contenedor_day_calendario" class="hidden fecha_calendario fecha_calendario_left">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masDayCalendarMobile" class="signo_calendario" onclick=Calendar.addDay()><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="dayCalendarMobile" class="numero_calendario">1</div>';
      template += '             </div>';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="menosDayCalendarMobile" class="signo_calendario" onclick=Calendar.removeDay()><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div id="contenedor_month_calendario" class="fecha_calendario">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.addMonth())><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="monthCalendarMobile" class="numero_calendario" style="margin-top: 20px">' + textMonthCalendarMobile + '</div>';
      template += '             </div>';
      template += '             <div id="anterior_month" class="contenedor_signo_calendario">';
      template += '                 <div id="menosMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.removeMonth())><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div id="contenedor_year_calendario" class="fecha_calendario fecha_calendario_right">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masYearCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.addYear())><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="yearCalendarMobile" class="numero_calendario" style="margin-top: 20px">' + yearCalendarMobile + '</div>';
      template += '             </div>';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="menosYearCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.removeYear())><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div style="clear:both;"></div>';
      template += '     </div>';
      template += '     </div>';
      template += ' </div></form>';

      return template;
    }

    return {
        categoryTranslate: categoryTranslate,
        getCategoryName  : getCategoryName,
        getCategoryId    : getCategoryId,
        getFormatDate    : getFormatDate,
        getElegantDate   : getElegantDate,
        getFormatNumber  : getFormatNumber,
        getTemplateSelectDayMonthYear : getTemplateSelectDayMonthYear, 
        getTemplateSelectMonthYear  : getTemplateSelectMonthYear,
        miclick          : miclick
    };

})(Lungo);